﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-cn" />
<meta name="author" content="EasyesESYS" />
<meta name="Copyright" content="Easyes.com.cn" />
<meta name="Description" content="易讯网络国内顶级企业建站软件开发商， 是目前中国ASP建站综合服务提供商之一。" />
<title>易讯建站管理系统(EasyesESYS)-http://www.Easyes.com.cn</title>
<link href="Images/index.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript"> 
function  change_url(i)
{
	switch(i)
	{
		case 1:
			window.location.href = '/index.asp';
			break;
		case 2:
			window.location.href = '/index2.asp';
			break;
		case 3:
			window.location.href = '/index3.asp';
			break;
	}
}
</script>
</head>
<body>
<div id="main">
<!--#include file="Head.asp"-->
<div class="banner left"><img alt="易讯建站系统" height="336" src="Images/value2.jpg" width="950" /></div>

<div class="content">
<ul class="list_intro">
<li class="on" onclick="change_url(1)">
<div class="tx"><p class="inst">易讯建站管理系统小巧 灵活 安全 快速 稳定。</p></div>
</li>
<li onclick="change_url(2)">
<div class="tx"><p class="inst">客户为准 完美为荣 用心服务　共创价值</p></div>
</li>
<li onclick="change_url(3)">
<div class="tx"><p class="inst">易讯网络专注于中小企业和个人建站服务。</p></div>
</li>
</ul>
</div>
<!--#include file="Bottom.asp"-->
</div>
</body>
</html>
