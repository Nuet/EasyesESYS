﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=3    '操作权限
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>管理首页</title>
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
</head>
<body>
<table border="0" cellSpacing="0" cellPadding="0" width="100%" align="center">
<tr><td background="Images/admin_main_01.gif" width="481"></td><td rowspan="2"></td></tr>
<tr><td><img src="Images/admin_main_02.gif" width="481" height="43"></td></tr>
<tr><td><img src="Images/admin_main_03.gif" width="481" height="35"></td>
<td align="right"><img src="Images/admin_main_04.gif" width="68" height="35"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; 系统信息</td></tr></table>
<table width="100%" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr> 
<td class="tdbgFont"><img border="0" src="Images/arrow_r.gif" width="7" height="7"><b> 帐户信息</b></td>
</tr>
<tr> 
<td valign="top" class="tdbg">
<table cellspacing="0" width="100%">
<tr> 
<td align="right" class="tdbg" width="100">帐号名称：</td>
<td class="tdbg" width="150"><%=session("Admin")%></td>
<td align="right" class="tdbg" width="80">网站名称：</td>
<td class="tdbg"><%=SiteName%></td>
</tr>
<tr> 
<td align="right" class="tdbg" width="100">隶属角色：</td>
<td class="tdbg" width="150"><%
select case session("purview")
case 1
	response.write "超级用户"
case 2
	response.write "高级管理员"
case 3
	response.write "信息录入员"
end select
%></td>
<td align="right" class="tdbg" width="80">网站网址：</td>
<td class="tdbg"><%=SiteUrl%></td>
</tr>
<tr> 
<td align="right" class="tdbg" width="100">最后登陆时间：</td>
<td class="tdbg" width="150"><%=session("LastLoginTime")%></td>
<td align="right" class="tdbg" width="80">安全验证：</td>
<td class="tdbg"><%
select case EnableSiteManageCode
case True
response.write "开启"
case False
response.write "关闭<font color=red>(建议开启)</font>"
end select
%></td>
</tr>
<tr> 
<td align="right" class="tdbg" width="100">最后登录IP：</td>
<td class="tdbg" width="150"><%=session("LastLoginIP")%></td>
<td align="right" class="tdbg" width="80">登录次数：</td>
<td class="tdbg"><%=session("LoginTimes")%></td>
</tr>
</table>
</td>
</tr>
</table>
<%
Dim WshShell,WshSysEnv,tnow,oknow,okCPUS, okCPU, okOS,isobj,WSshell,fsoobj
ObjTest("Scripting.FileSystemObject")
if isobj then
	set fsoobj=server.CreateObject("Scripting.FileSystemObject")
%><table width="100%" cellpadding="4" cellspacing="1" class="tabBgColor">
<tr>
<td class="tdbgFont">
<img border="0" src="Images/arrow_r.gif"><b> 使用本系统必须支持</b></td>
</tr>
<tr>
<td class="tdbg">
<table height="94" border="0" cellpadding="0" cellspacing="0">
<tr><td align="right" height="22"><strong style="font-weight: 400">JRO.JetEngine(ACCESS)：</strong></td>
<td height="22"><%
On Error Resume Next
Server.CreateObject("JRO.JetEngine")
if err=0 then
response.write("<font color=#0076AE>√</font>")
else
response.write("<font color=red>×</font>")
end if
err=0
%></td></tr>
<tr><td align="right" height="22"><strong style="font-weight: 400">
数据库使用：</strong></td>
<td height="22"><%
On Error Resume Next
Server.CreateObject("adodb.connection")
if err=0 then
response.write("<font color=#0076AE>√,可以使用本系统</font>")
else
response.write("<font color=red>×,不能使用本系统</font>")
end if
err=0
%></td></tr>
<tr><td align="right" height="22"><strong style="font-weight: 400">
FSO文本文件读写：</strong></td>
<td height="22"><%
On Error Resume Next
Server.CreateObject("Scripting.FileSystemObject")
if err=0 then
response.write("<font color=#0076AE>√,可以使用本系统</font>")
else
response.write("<font color=red>×，不能使用此系统</font>")
end if
err=0
%></td></tr>
<tr><td align="right" height="22"><strong style="font-weight: 400">Microsoft.XMLHTTP：</strong></td>
<td height="22"><%If Not IsObjInstalled(theInstalledObjects(22)) Then%>
<font color="red">
×</font>
<%else%>
<font color="0076AE"> 
√</font>
<%end if%>
(非必须) 　Adodb.Stream
<%If Not IsObjInstalled(theInstalledObjects(23)) Then%>
<font color="red">
×</font>
<%else%>
<font color="0076AE"> 
√</font>
<%end if%></td>
</tr><tr>
<td align="right" height="22">
<strong style="font-weight: 400">客户端浏览器版本 ：</strong></td>
<td height="22"><%
Dim Agent,Browser,version,tmpstr
Agent=Request.ServerVariables("HTTP_USER_AGENT")
Agent=Split(Agent,";")
If InStr(Agent(1),"MSIE")>0 Then
Browser="MS Internet Explorer "
version=Trim(Left(Replace(Agent(1),"MSIE",""),6))
ElseIf InStr(Agent(4),"Netscape")>0 Then
Browser="Netscape "
tmpstr=Split(Agent(4),"/")
version=tmpstr(UBound(tmpstr))
ElseIf InStr(Agent(4),"rv:")>0 Then
Browser="Mozilla "
tmpstr=Split(Agent(4),":")
version=tmpstr(UBound(tmpstr))
If InStr(version,")") > 0 Then
tmpstr=Split(version,")")
version=tmpstr(0)
End If
End If
response.Write(""&Browser&"  "&version&"")
%></td></tr>
</table>
</td>
</tr>
</table>
<table width="100%" cellpadding="4" cellspacing="1" bgcolor="#E8E8E8" class="tabBgColor">
<tr>
<td bgcolor="#F6f6f6" class="tdbgFont">
<img border="0" src="Images/arrow_r.gif" width="7" height="7"><b> 当前文件夹信息</b>[<a href="javascript:window.location.reload()"><font color="#FF6600">刷新</font></a>]</td>
</tr>
<tr>
<td width="91%" class="tdbg"> 
<table width="100%"  border="0" cellspacing="0" cellpadding="2">
<tr>
<td>&nbsp;
<%
	Dim dPath,dDir,dDrive
	dPath = server.MapPath("./")
	set dDir = fsoObj.GetFolder(dPath)
	set dDrive = fsoObj.GetDrive(dDir.Drive)
%>
文件夹: <%=dPath%> </td>
</tr>
</table>
<table width="100%" border=0 cellpadding=3 cellspacing=1 class="tabBgColor">
<tr align="center" height="18">
<td class="tdbgFont">可用空间</td>
<td class="tdbgFont">已用空间</td>
<td class="tdbgFont">文件夹数</td>
<td class="tdbgFont">文件数</td>
<td class="tdbgFont">创建时间</td>
</tr>
<tr align="center">
<td class="tdbg"><%=cSize(dDrive.AvailableSpace)%></td>
<td class="tdbg"><%=cSize(dDir.Size)%></td>
<td class="tdbg"><%=dDir.SubFolders.Count%></td>
<td class="tdbg"><%=dDir.Files.Count%></td>
<td class="tdbg"><%=dDir.DateCreated%></td>
</tr>
</table>
</td>
</tr>
</table>
<%End if%>
<table class="tableBorder" cellspacing="1" cellpadding="3" width="100%" border="0" bgcolor="#CCCCCC">
<tr>
<td width="68" height="23" bgcolor="#F4F5F6" class="tdbgFont">当前版本</td>
<td bgcolor="#F4F5F6" class="tdbg"><strong style="color: blue">易讯建站管理系统:EasyesESYS <%=SystemVer%></strong></td>
</tr>
<tr>
<td height="23" bgcolor="#FFFFFF" class="tdbgfont">版权声明</td>
<td style="line-height: 150%" bgcolor="#FFFFFF" class="tdbg">　1、易讯建站管理系统，英文名称为&quot;EasyesESYS&quot;，系易讯网络(Easyes.com.cn)独立开发，并受中华人民共和国法律保护<br>
　2、本软件为商业授权软件,未经书面授权，不得向任何第三方提供本系统；如需用于商业，必须注册或购买本系统商业版本<br />
　2、用户自由选择是否使用,在使用中出现任何问题和由此造成的一切损失作者将不承担任何责任<br />
　4、您可以对本系统进行修改和美化，但必须保留完整的版权信息<br />
　5、本软件受中华人民共和国《着作权法》《计算机软件保护条例》等相关法律、法规保护，作者保留一切权利</td>
</tr>
</table>
</body>
</html>
<%
' 获取服务器常用参数
sub getsysinfo()
  on error resume next
  Set WshShell = server.CreateObject("WScript.Shell")
  Set WshSysEnv = WshShell.Environment("SYSTEM")
  okOS = cstr(WshSysEnv("OS"))
  okCPUS = cstr(WshSysEnv("NUMBER_OF_PROCESSORS"))
  okCPU = cstr(WshSysEnv("PROCESSOR_IDENTIFIER"))
  if isnull(okCPUS) & "" = "" then
    okCPUS = Request.ServerVariables("NUMBER_OF_PROCESSORS")
  end if
end sub
'检查组件是否被支持及组件版本的子程序
sub ObjTest(strObj)
  on error resume next
  IsObj=false
  VerObj=""
  set TestObj=server.CreateObject (strObj)
  If -2147221005 <> Err then		'感谢网友iAmFisher的宝贵建议
    IsObj = True
    VerObj = TestObj.version
    if VerObj="" or isnull(VerObj) then VerObj=TestObj.about
  end if
  set TestObj=nothing
End sub
' 转换字节数为简写形式
function cSize(tSize)
  if tSize>=1073741824 then
    cSize=int((tSize/1073741824)*1000)/1000 & " GB"
  elseif tSize>=1048576 then
    cSize=int((tSize/1048576)*1000)/1000 & " MB"
  elseif tSize>=1024 then
    cSize=int((tSize/1024)*1000)/1000 & " KB"
  else
    cSize=tSize & "B"
  end if
end function
Function IsObjInstalled(strClassString)
On Error Resume Next
IsObjInstalled = False
Err = 0
Dim xTestObj
Set xTestObj = Server.CreateObject(strClassString)
If 0 = Err Then IsObjInstalled = True
Set xTestObj = Nothing
Err = 0
End Function
%>