﻿<%@language=vbscript codepage=65001%>
<%
option explicit
response.buffer=true	
Const PurviewLevel=3    '操作权限 1超级用户2高级管理员3信息录入员
'判断管理是否登陆
'**************************************************************
if session("admin")="" then
    response.Redirect "../Admin_Logout.asp"
else
	if session("purview")>PurviewLevel then
		response.write "<br><p align=center><font color='red' size='2'>您没有操作的权限</font></p>"
		response.end
	end if
end if
%>
<!--#include file="../Config/Config.asp"-->
<!--#include file="../Config/Version.asp"-->
<html>
<head>
<title>文件上传</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../Images/Style.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
BODY{
BACKGROUND-COLOR: #E1F4EE;
font-size:9pt
}
-->
</style>
<script type="text/javascript" language=javascript>
function check()
{
var strFileName=form1.FileName.value;
var FileType;
var ImgWH;
if (strFileName=="")
{
alert("请选择要上传的文件");
return false;
}
}
</script>
</head>
<iframe src="" name="hiddenForm" width=0 height=0 frameborder="0"></iframe>
<body bgcolor="buttonface" leftmargin="0" topmargin="0" scroll="no" style="background-color: #ECE9D8">
<table border="0" cellspacing="0" cellpadding="3" height="165" width="398">
<tr>
<td bgcolor="#FFFFFF" align="center">
<img src="UpFileLogo.gif" width="130" height="131">
</td>
<td height="165" valign="top">
<table width="254" border="0" cellpadding="0" cellspacing="0">
<tr>
<td height="2" width="254"><font size="2"><b>文件上传</b></font>:</td>
</tr>
<tr>
<td valign="bottom" width="254">
<table border="0" cellpadding="0" cellspacing="0" width="100%" id="table1">
<tr>
<form action="upfile.asp" method="post" name="form1" onSubmit="return check()" enctype="multipart/form-data" target="hiddenForm">
<td>
<input name="FileName" type="File" class="input_text" size="18">
<input type="submit" name="Submit" value="上传" class="button01-out"><input name="ImgWidth" type="hidden" id="ImgWidth"><input name="ImgHeight" type="hidden" id="ImgHeight">
</td>
</form>
</tr>
</table>
</td>
</tr>
<tr>
<td height="2" width="254">
<hr size="1">
</td>
</tr>
<tr>
<td height="54" valign="bottom" width="254">
<li>文件上传速度较慢，请耐心等侍</li>
<li>大小不得超过<font face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"><%=MaxFileSize%>K</font></li>
<li>允许上传文件类型：<font face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"><%=UpFileType%></font><li>图片目录存放在<font face="Verdana, Arial, Helvetica, sans-serif" color="#FF0000"><%=SaveUpFilesPath%></font><font face="Verdana, Arial, Helvetica, sans-serif">文件夹</font></td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>
