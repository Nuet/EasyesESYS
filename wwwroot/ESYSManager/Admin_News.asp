﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=3    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/ClassList_Fun.asp"-->
<html>
<head>
<title>常规资源管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<script type="text/javascript" language=javascript src="Config/UpFils.js"></SCRIPT>
<script type="text/javascript" src="Config/ClassTree.js"></script>
<script type="text/javascript" src="Config/Tooltip.js"></script>
<script type="text/javascript" src="Config/InfoList.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; <a href="Admin_News.asp">常规资源管理</a></td></tr></table><%
Dim Action,ParentID,i,FoundErr,ErrMsg
Action=trim(request("Action"))
if Action="Add" then
	call AddNews()
elseif Action="SaveAdd" then
	call SaveAdd()
elseif Action="Modify" then
	call Modify()
elseif Action="SaveModify" then
	call SaveModify()
else
	call main()
end if
if FoundErr=True then
	call WriteErrMsg(ErrMsg)
end if
call CloseConn()
Sub main()
%>
<input name="Submit32" type="button" class="button01-out" value="添加文章" onClick="window.location='Admin_News.asp?Action=Add'" title="添加最新的常规资源信息">
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr> 
<td width="100%" bgcolor="#f6f6f6" class="tdbg">当前位置：<%
Dim ParentID
	ParentID=trim(request("ParentID"))
if ParentID="" then
	ParentID=0
else
	ParentID=CLng(ParentID)
end if
Dim sType
    sType=Replace(Request("sType"),"'","")
    If sType="" Then
        sType="Title"
    End If
Dim sKey
    sKey=jencode(Replace(Request("sKey"),"'",""))
	Response.write ""&GetClassPath1(ParentID)&""
%></td>
</tr>
<tr> 
<td width="100%" bgcolor="#f6f6f6" class="tdbg">
<%=GetClassPath2(ParentID,2)%></td>
</tr>
</table><%
Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql="SELECT * From [All_News] Where "&sType&" Like '%"&sKey&"%' And ClassID In ("& ParentID&AllChildClass(ParentID) &") Order By ArticleID DESC"
 	Rs.PageSize=20
	Rs.CacheSize=Rs.PageSize
    Rs.Open Sql,Conn,1,1
Dim CurrentPage
    If Request("CurrentPage")="" Then
        CurrentPage=1
    Else
        CurrentPage=Request("CurrentPage")
    End If    
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If
%>
<form name="form2" method="post" action="Admin_NewsMdy.asp">
<table border="0" cellpadding="2" cellspacing="1" class="tabBgColor" width="100%">
<tr height="22"> 
<td align="center" class="tabheadfont" width="40">ID</td>
<td align="center" class="tabheadfont" width="30">选择</td>
<td align="center" class="tabheadfont">资源标题</td>
<td align="center" class="tabheadfont" width="40">点击</td>
<td align="center" class="tabheadfont" width="100">所属主类别</td>
<td align="center" class="tabheadfont" width="40">审核</td>
<td align="center" class="tabheadfont" width="40">操作</td>
</tr>
<%
  Dim I
  For I=1 To Rs.PageSize
      If Rs.Eof Then
        Exit For
    End If
%><tr class="tdbg">
<td align="center"><%=Rs("ArticleID")%></td>
<td align="center"><input type="checkbox" name="Id" value="<%=Rs("ArticleID")%>" title="记录编号：<%=Rs("ArticleID")%>" HaveChecked="<%=CBool(Rs("Passed"))%>"></td>
<td><img border="0" src="Images/arrow_a.gif"><%
  If Rs("TitleUrl")<>"" Then
    Response.Write "<a href="""&Rs("TitleUrl")&""" target=""_blank"" title='责任编辑:"&Rs("Editor")&"'>"&TitleSytle(Rs("TitleFontColor"),Rs("TitleFontType"),juncode(Rs("Title")))&"</a>"
  Else
    Response.Write "<a href=""Admin_News.asp?Action=Modify&ArticleID=" & rs("ArticleID") &""" title='责任编辑:"&Rs("Editor")&"'>"&TitleSytle(Rs("TitleFontColor"),Rs("TitleFontType"),juncode(Rs("Title")))&"</a>"
  End If
%><label title="更新时间:<%=FormatDateTime(Rs("UpdateTime"),1)%>" style="cursor:hand"><font color="#808080">[<%=Rs("Date")%>]</font></label></td>
<td align="center"><%=Rs("Count")%></td>
<td align="center"><%=Rs("ClassName")%></td>
<td align="center"><%
    If CBool(Rs("Passed")) Then
        Response.write("<a href=""Admin_NewsMdy.asp?Work=CheckReco&Id="& Rs("ArticleID") &""">已审</a>")
    Else
        Response.write("<a href=""Admin_NewsMdy.asp?Work=CheckReco&Id="& Rs("ArticleID") &"""><font color='green'>未审</font></a>")
    End If
%></td>
<td align="center"><%
	Response.write "<a href='Admin_News.asp?Action=Modify&ArticleID=" & rs("ArticleID") &"'>编辑</a>"
%></td>
</tr>
<%
  Rs.MoveNext
  Next
%></table> 
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr> 
<td align="right" bgcolor="#f6f6f6">
<script type="text/javascript" src="Config/PageList.js"></script>
<script type="text/javascript" language="JavaScript">
	<!--
	var plb = new PageListBar('plb',<%=Rs.PageCount%>,<%=CurrentPage%>,'&sType=<%=sType%>&sKey=<%=sKey%>&ParentID=<%=ParentID%>',20);
	document.write(plb);
	//-->
</script>
</td>
</tr>
<tr>
<td align="right" class="tdbg">
<input type="hidden" name="Work">
<input type="hidden" name="RealDel">
<label for="selectAllReco"> 
<input type="checkbox" name="checkbox2" value="checkbox" id="selectAllReco" onclick="selectAllCheckBox(form2.Id)">反选</label> 
<input name="Submit" type="button" class="button01-out" value="审 核" title="批量审核" onClick="CheckReco(form2,event.srcElement)">
<input name="Submit" type="button" class="button01-out" value="删 除" title="批量删除" onClick="DeleteReco(form2,event.srcElement)">
</td>
</tr>
</table>
</form>
<form name="form1" method="post" action="?">
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr><td width="67%" align="right" class="tdBg">
<input name="Work" type="hidden" id="Work" value="">搜索: 
<select name="sType" class="input_text">
<option value="Title">资源标题</option>
<option value="Author">作者名称</option>
</select><input name="ParentID" type="radio" class="Input" value="0" checked>所有 <input name="ParentID" type="radio" class="Input" value="<%=ParentID%>">当前 </td>
<td width="25%" align="right" class="tdBg"><input name="sKey" type="text" class="input_text" id="sKey" style="width:100%" value="<%=Trim(Request("sKey"))%>"></td>
<td width="5%" align="center" class="tdBg"><input name="SearchButton" type="submit" class="button02-out" value="确  定"></td>
</tr>
</table>
</form>
<%
Rs.Close
Set Rs=Nothing
End sub
Sub AddNews()
	Call Ajax()
'添加资源开始
    Dim CurrentClassID,CurrentAuthor
    CurrentClassID=Request.Cookies("EasyesESys")("CurrentClassID")
    CurrentAuthor=Request.Cookies("EasyesESys")("CurrentAuthor")
    If Not IsNumeric(CurrentClassID) Then
        CurrentClassID = -1
    Else
        CurrentClassID = CLng(CurrentClassID)
    End If
%>
<form name="myform1" method="post" action="Admin_News.asp" class="required-validate">
<input name="Action" type="hidden" id="Action" value="SaveAdd">
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="tabBgColor">
<tr>
<td class="tabheadfont" colspan="2">添加常规资源内容</td>
</tr>
<tr>
<td valign="top" class="tdbgfont" align="right">资源类别:</td>
<td valign="top" class="tdbg">
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr><td width="68%">
<span id="for-validate-one-required-form">
<script type="text/javascript" language="javascript">
var root1
root1=CreateRoot("myTree1","·请选择[资源类别]")
<%Call CreateClassTree1(0,CurrentClassID,2)%>
</script>
</span></td>
<td width="32%" align="right" valign="top">
<label for="CurrentClassID">
<input type="checkbox" id="CurrentClassID" name="CurrentClassID" value="1" <%If CurrentClassID<>-1 Then Response.Write "checked" End If%>><font color="#666666">记忆</font></label>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td class="tdbgfont" align="right">资源标题:</td>
<td height="25" class="tdbgfont">
<input name="Title" type="text" id="ClassName" size="50" class="input_text required" style="background-image: url('Images/Rule.gif')">
<select name="TitleFontColor" id="TitleFontColor" class=input_text>
<option value="">颜色</option>
<OPTION value="">默认</OPTION>
<OPTION value="#000000" style="background-color:#000000"></OPTION>
<OPTION value="#FFFFFF" style="background-color:#FFFFFF"></OPTION>
<OPTION value="#008000" style="background-color:#008000"></OPTION>
<OPTION value="#800000" style="background-color:#800000"></OPTION>
<OPTION value="#808000" style="background-color:#808000"></OPTION>
<OPTION value="#000080" style="background-color:#000080"></OPTION>
<OPTION value="#800080" style="background-color:#800080"></OPTION>
<OPTION value="#808080" style="background-color:#808080"></OPTION>
<OPTION value="#FFFF00" style="background-color:#FFFF00"></OPTION>
<OPTION value="#00FF00" style="background-color:#00FF00"></OPTION>
<OPTION value="#00FFFF" style="background-color:#00FFFF"></OPTION>
<OPTION value="#FF00FF" style="background-color:#FF00FF"></OPTION>
<OPTION value="#FF0000" style="background-color:#FF0000"></OPTION>
<OPTION value="#0000FF" style="background-color:#0000FF"></OPTION>
<OPTION value="#008080" style="background-color:#008080"></OPTION>
</select>
<select name="TitleFontType" id="TitleFontType" class=input_text>
<option value="0">字形</option>
<option value="1">粗体</option>
<option value="2">斜体</option>
<option value="3">粗+斜</option>
</select>
</td>
</tr>
<tr>
<td class="tdbgfont" align="right">标题连接:</td>
<td height="25" class="tdbg"><input name="TitleUrl" type="text" id="TitleUrl" value="" size="60" class="input_text"  onmouseover="showToolTip('注意：不使用资源跳转功能时请将此项置空',event.srcElement)" onmouseout="hiddenToolTip()"></td>
</tr>
<tr>
<td class="tdbgfont" align="right">责任编辑:</td>
<td height="25" class="tdbgfont"><input name="Editor" type="text" id="Editor" readonly value="<%=session("Admin")%>" class="input_text required" size="15"></td>
</tr>
<tr>
<td class="tdbgfont" align="right">资源作者:</td>
<td height="25" class="tdbgfont"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
<tr><td width="68%">
<input name="Author" type="text" id="Author" size="15" class="input_text required" value="<%=CurrentAuthor%>">
<select name="font2" onChange="Author.value=this.value;" class="input_text">
<option value="" selected>选择作者</option>
<option value="佚名">佚名</option>
<option value="本站">本站</option>
<option value="不详">不详</option>
<option value="未知">未知</option>
</select>
</td>
<td width="32%" align="right" valign="top">
<label for="CurrentAuthor"><input type="checkbox" id="CurrentAuthor" name="CurrentAuthor" value="1" <%If CurrentAuthor<>"" Then Response.Write "checked" End If%>><font color="#666666">记忆</font></label>
</td>
</tr>
</table> 
</td>
</tr>
<tr>
<td class="tdbgfont" valign="top" align="right">资源特性:</td>
<td class="tdbgfont" valign="top">
<script>
var root3
root3=CreateRoot("myTree3","·请选择[资源特性]")
<%Call SpecialityList_Add(0,2)%>
</script>
</td>
</tr>
<tr> 
<td height="22" class="tdbgfont" bgcolor="#F6F6F6" colspan="2"><font color="#0000FF">资源内容</font>:<span style="cursor:hand" Id="ShowHiddenHtmlEdit" Title="显示/隐藏" onClick="if(trNewsContent.style.display==''){trNewsContent.style.display='none';ShowHiddenHtmlEdit.innerHTML='[显示]'}else{trNewsContent.style.display='';ShowHiddenHtmlEdit.innerHTML='[隐藏]'}">[隐藏]</span></td>
</tr>
<tr id="trNewsContent"> 
<td valign="top" colspan="2" class="tdbgFont"><%
Response.write"<textarea name=""Content"" style=""display:none""></textarea>"&chr(13)
call Admin_HtmlEditor()
%></td>
</tr>
<tr>
<td align="right" height="25" class="tdbgfont">引导图片:</td>
<td height="25" class="tdbg">
<input name="Images" type="text" id="Images" size="55" class="input_text"> 
<input type="button" class="button01-out" value="上传" onClick="OpenWindowAndSetValue('UpFile/UpLoad.Asp',300,130,window,document.myform1.Images);"></td>
</tr>
<tr>
<td align="right" height="53" class="tdbgfont">内容摘要:</td>
<td class="tdbg">
<textarea name="Text" id="Text" style="width:400 ; height:76" class="input_text required"></textarea></td>
</tr>
<tr>
<td width="85" align="right" class="tdbg"></td>
<td height="25" class="tdbg">
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_News.asp'"></td>
</tr>
</table>
</form>
<%
End sub
Sub Modify
	Call Ajax()
Dim ArticleID
ArticleID=Request("ArticleID")
Dim Sql
    Sql="Select * From [Easyes_News] Where ArticleID=" & ArticleID
Dim Rs
Set Rs=Conn.ExeCute(Sql)
%>
<form name="myform" method="post" action="Admin_News.asp" class="required-validate">
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="tabBgColor">
<tr>
<td class="tabheadfont" colspan="2">编辑常规资源内容</td>
</tr>
<tr>
<td valign="top" class="tdbgfont" align="right">资源类别:</td>
<td valign="top" class="tdbg">
<script language="javascript">
var root2
root2=CreateRoot("myTree2","·请选择[资源类别]")
<%Call CreateClassTree2(0,Rs("ClassID"),2)%>
</script>
</td>
</tr>
<tr>
<td class="tdbgfont" align="right">资源标题:</td>
<td class="tdbgfont">
<input name="Title" type="text" id="ClassName" size="50" class="input_text required" value="<%=juncode(Server.HTMLEncode(Rs("Title")))%>" style="background-image: url('Images/Rule.gif')">
<select name="TitleFontColor" id="TitleFontColor" class=input_text>
<option value="" <%if rs("TitleFontColor")="" then response.write " selected"%>>颜色</option>
<OPTION value="">默认</OPTION>
<OPTION value="#000000" style="background-color:#000000" <%if rs("TitleFontColor")="#000000" then response.write " selected"%>></OPTION>
<OPTION value="#FFFFFF" style="background-color:#FFFFFF" <%if rs("TitleFontColor")="#FFFFFF" then response.write " selected"%>></OPTION>
<OPTION value="#008000" style="background-color:#008000" <%if rs("TitleFontColor")="#008000" then response.write " selected"%>></OPTION>
<OPTION value="#800000" style="background-color:#800000" <%if rs("TitleFontColor")="#800000" then response.write " selected"%>></OPTION>
<OPTION value="#808000" style="background-color:#808000" <%if rs("TitleFontColor")="#808000" then response.write " selected"%>></OPTION>
<OPTION value="#000080" style="background-color:#000080" <%if rs("TitleFontColor")="#000080" then response.write " selected"%>></OPTION>
<OPTION value="#800080" style="background-color:#800080" <%if rs("TitleFontColor")="#800080" then response.write " selected"%>></OPTION>
<OPTION value="#808080" style="background-color:#808080" <%if rs("TitleFontColor")="#808080" then response.write " selected"%>></OPTION>
<OPTION value="#FFFF00" style="background-color:#FFFF00" <%if rs("TitleFontColor")="#FFFF00" then response.write " selected"%>></OPTION>
<OPTION value="#00FF00" style="background-color:#00FF00" <%if rs("TitleFontColor")="#00FF00" then response.write " selected"%>></OPTION>
<OPTION value="#00FFFF" style="background-color:#00FFFF" <%if rs("TitleFontColor")="#00FFFF" then response.write " selected"%>></OPTION>
<OPTION value="#FF00FF" style="background-color:#FF00FF" <%if rs("TitleFontColor")="#FF00FF" then response.write " selected"%>></OPTION>
<OPTION value="#FF0000" style="background-color:#FF0000" <%if rs("TitleFontColor")="#FF0000" then response.write " selected"%>></OPTION>
<OPTION value="#0000FF" style="background-color:#0000FF" <%if rs("TitleFontColor")="#0000FF" then response.write " selected"%>></OPTION>
<OPTION value="#008080" style="background-color:#008080" <%if rs("TitleFontColor")="#008080" then response.write " selected"%>></OPTION>
</select>
<select name="TitleFontType" id="TitleFontType" class=input_text>
<option value="0" <%if rs("TitleFontType")="0" then response.write " selected"%>>字形</option>
<option value="1" <%if rs("TitleFontType")="1" then response.write " selected"%>>粗体</option>
<option value="2" <%if rs("TitleFontType")="2" then response.write " selected"%>>斜体</option>
<option value="3" <%if rs("TitleFontType")="3" then response.write " selected"%>>粗+斜</option>
</select></td>
</tr>
<tr>
<td class="tdbgfont" align="right">标题连接:</td>
<td height="25" class="tdbg">
<input name="TitleUrl" type="text" id="TitleUrl" value="<%=Rs("TitleUrl")%>" size="60" class="input_text"  onmouseover="showToolTip('注意：不使用资源跳转功能时请将此项置空',event.srcElement)" onmouseout="hiddenToolTip()"></td>
</tr>
<tr>
<td class="tdbgfont" align="right">责任编辑:</td>
<td height="25" class="tdbgfont">
<input name="Editor" type="text" id="Editor0" readonly value="<%=session("Admin")%>" class="input_text required" size="15"></td>
</tr>
<tr>
<td class="tdbgfont" align="right">资源作者:</td>
<td height="25" class="tdbgfont">
<input name="Author" type="text" id="Author" size="15" class="input_text required" value="<%=juncode(Rs("Author"))%>">
<select name="font2" onChange="Author.value=this.value;" class="input_text">
<option value="" selected>选择作者</option>
<option value="佚名">佚名</option>
<option value="本站">本站</option>
<option value="不详">不详</option>
<option value="未知">未知</option>
</select>
</td>
</tr>
<tr>
<td class="tdbgfont" valign="top" align="right">资源特性:</td>
<td class="tdbgfont" valign="top">
<script>
var root4
root4=CreateRoot("myTree4","·请选择[资源特性]")
<%Call SpecialityList_Mdy(0,Rs("SpecialID"),2)%>
</script></td>
</tr>
<tr> 
<td height="22" class="tdbgfont" bgcolor="#F6F6F6" colspan="2"><font color="#0000FF">资源内容</font>:<span style="cursor:hand" Id="ShowHiddenHtmlEdit" Title="显示/隐藏" onClick="if(trNewsContent.style.display==''){trNewsContent.style.display='none';ShowHiddenHtmlEdit.innerHTML='[显示]'}else{trNewsContent.style.display='';ShowHiddenHtmlEdit.innerHTML='[隐藏]'}">[隐藏]</span></td>
</tr>
<tr id="trNewsContent"> 
<td valign="top" colspan="2" class="tdbgFont"><%
Response.write"<textarea name=""Content"" style=""display:none"">"&Server.HTMLEncode(Rs("Content"))&"</textarea>"&chr(13)
call Admin_HtmlEditor()
%></td>
</tr>
<tr>
<td align="right" height="11" class="tdbgfont">引导图片:</td>
<td height="11" class="tdbg">
<input name="Images" type="text" id="Images" size="55" class="input_text" value="<%=Rs("Images")%>"> 
<input type="button" class="button01-out" value="上传" onClick="OpenWindowAndSetValue('UpFile/UpLoad.Asp',300,130,window,document.myform.Images);"></td>
</tr>
<tr>
<td align="right" height="53" class="tdbgfont">内容摘要:</td>
<td class="tdbg">
<textarea name="Text" id="Text" style="width:400px;height:76px" class="input_text required"><%=Server.HTMLEncode(Rs("Text"))%></textarea></td>
</tr>
<tr>
<td width="85" align="right" class="tdbg">
<input name="ArticleID" type="hidden" id="ArticleID" value="<%=Rs("ArticleID")%>">
<input name="Action" type="hidden" id="Action" value="SaveModify">
</td>
<td height="25" class="tdbg">
<input name="SaveAddButton" id="SaveAddButton" class="button02-out" type="submit" value=" 保 存 ">
<input name="Submit2" class="button02-out" type="reset" value=" 还 原 ">
<input name="Submit2" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_News.asp'"></td>
</tr>
</table>
</form><%
Rs.Close
Set Rs=Nothing
End Sub
%></body></html><%
sub SaveAdd()
'添加
Dim ClassID,Title,TitleFontColor,TitleFontType,TitleUrl,Author,Editor,Special,Content,Images,Text
    '取得参数
    ClassID=Request.Form("radioBoxItem")
	Title=HTMLDecode(trim(Request.Form("Title")))
Title=jencode(Title) '日语处理
	TitleFontColor=trim(Request.Form("TitleFontColor"))
	TitleFontType=trim(Request.Form("TitleFontType"))
    TitleUrl=trim(Request.Form("TitleUrl"))
    Author=HTMLDecode(trim(Request.Form("Author")))
Author=jencode(Author) '日语处理
    Editor=HTMLDecode(trim(Request.Form("Editor")))
	Special=Replace(Request("Speciality")," ","")
	Content=trim(Request.Form("Content"))
	Images=trim(Request.Form("Images"))
	Text=trim(Request.Form("Text"))
if ClassID="" then
	FoundErr=True
	ErrMsg=ErrMsg & "·请选择[资源类别]！\n"
end if
if FoundErr=True then
	Exit sub
end if
If sSaveFileSelect="Yes" Then
	Content=ReplaceRemoteUrl(Content,sSaveFilePath,sFileExt)
End If
Dim rs
	Set rs=Server.CreateObject("Adodb.RecordSet")
	rs.open "Select * From Easyes_News Where Title='" & Title & "'",conn,1,3
	if not (rs.bof and rs.EOF) then
		FoundErr=True
		ErrMsg=ErrMsg & "·此文章已经存在！\n"
		rs.close
	    set rs=Nothing
    	exit sub
	End if
    rs.addnew
	rs("ClassID")=ClassID
	rs("SpecialID")=Special
	rs("Title")=Title
	rs("TitleUrl")=TitleUrl
	rs("Author")=Author
	rs("Editor")=Editor
	rs("Content")=Content
If Images="" Then
	rs("Images")=GetImg(RegExpExecute(Content))
Else
	rs("Images")=Images
End If		
	rs("Text")=Text
    rs("TitleFontColor")=TitleFontColor
	rs("TitleFontType")=TitleFontType
If AddChecked="Yes" Then
	Rs("Passed")=False
Else
	Rs("Passed")=True
End If
	rs("UpdateTime")=Now()
	rs.update
    rs.Close
    set rs=Nothing
'变量记录
    If Request("CurrentClassID")="1" Then
        Response.Cookies("EasyesESys")("CurrentClassID")=ClassID
    Else
        Response.Cookies("EasyesESys")("CurrentClassID")=""
    End If

    If Request("CurrentAuthor")="1" Then
        Response.Cookies("EasyesESys")("CurrentAuthor")=Author
    Else
        Response.Cookies("EasyesESys")("CurrentAuthor")=""
    End If
	Response.Redirect "Admin_News.asp?ClassID="&ClassID&""  
End sub

'修改
Sub SaveModify()
Dim ClassID,ArticleID,Title,TitleFontColor,TitleFontType,TitleUrl,Author,Editor,Special,Content,Images,Text
    '取得参数
    ClassID=Request.Form("radioBoxItem")
	ArticleID=trim(request.Form("ArticleID"))
	Title=HTMLDecode(trim(Request.Form("Title")))
Title=jencode(Title) '日语处理
	TitleFontColor=trim(Request.Form("TitleFontColor"))
	TitleFontType=trim(Request.Form("TitleFontType"))
    TitleUrl=trim(Request.Form("TitleUrl"))
    Author=HTMLDecode(trim(Request.Form("Author")))
Author=jencode(Author) '日语处理
    Editor=HTMLDecode(trim(Request.Form("Editor")))
	Special=Replace(Request("Speciality")," ","")
	Content=trim(Request.Form("Content"))
	Images=trim(Request.Form("Images"))
	Text=trim(Request.Form("Text"))
if ClassID="" then
	FoundErr=True
	ErrMsg=ErrMsg & "·请选择资源类别！\n"
end if
if FoundErr=True then
	Exit sub
end if
If IsNumeric(ArticleID) = False Then
	FoundErr=True
	ErrMsg=ErrMsg & "·请通过页面上的链接进行操作！\n"
	Exit sub
End If
If sSaveFileSelect="Yes" Then  '内容中文件处理
	Content=ReplaceRemoteUrl(Content,sSaveFilePath,sFileExt)
End If
Dim rs
	Set rs=Server.CreateObject("Adodb.RecordSet")
Dim Sql
	sql="Select * From Easyes_News Where ArticleID=" & ArticleID
	Set rs=Server.CreateObject("Adodb.RecordSet")
	rs.open sql,conn,1,3
if rs.bof and rs.EOF then
		FoundErr=True
		ErrMsg=ErrMsg & "·找不到指定的资源信息！\n"
		Exit sub
	rs.close
	set rs=Nothing
else
	rs("ClassID")=ClassID
	rs("SpecialID")=Special
	rs("Title")=Title
	rs("TitleUrl")=TitleUrl
	rs("Author")=Author
	rs("Editor")=Editor
	rs("Content")=Content
If Images="" Then
	rs("Images")=GetImg(RegExpExecute(Content))
Else
	rs("Images")=Images
End If
	rs("Text")=Text
    rs("TitleFontColor")=TitleFontColor
	rs("TitleFontType")=TitleFontType
If AddChecked="Yes" Then
	Rs("Passed")=False
Else
	Rs("Passed")=True
End If
	rs("UpdateTime")=Now()
	rs.update
    rs.Close
    set rs=Nothing
End If
	Response.Redirect "Admin_News.asp?ClassID="&ClassID&""  
End sub
%>