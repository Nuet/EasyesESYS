﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
response.buffer=true	
Const PurviewLevel=3    '操作权限
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/md5.asp"--><%
dim Action,UserName,password,PwdConfirm,FoundErr,ErrMsg
Action=trim(request("Action"))
UserName=trim(session("Admin"))
Dim sql
sql="Select * from Easyes_Manage where UserName='" & UserName & "'"
Dim rs
Set rs=Server.CreateObject("Adodb.RecordSet")
rs.Open sql,conn,1,3
if rs.Bof and rs.EOF then
	FoundErr=True
	ErrMsg=ErrMsg & "<br><li>不存在此用户！</li>"
else
if Action="Modify" then
	call ModifyPwd()
else
	call main()
end if
end if
rs.close
set rs=nothing
if FoundErr=True then
	call WriteErrMsg()
end if
call CloseConn()
Sub ModifyPwd()
	dim password,PwdConfirm,Password64
	password=trim(Request("Password"))
	PwdConfirm=trim(request("PwdConfirm"))
	if password="" then
		FoundErr=True
		ErrMsg=ErrMsg & "<br><li>新密码不能为空！</li>"
	end if
	if PwdConfirm<>Password then
		FoundErr=True
		ErrMsg=ErrMsg & "<br><li>确认密码必须与新密码相同！</li>"
		exit sub
	end if
	UserName=rs("UserName")
	If Password<>"" then
		rs("password")=md5(CfsEnCode(password))
	End if
   	rs.update
	Call WriteSuccessMsg("修改密码成功！下次登录时记得换用新密码哦！")
End sub
Sub main()
%>
<html>
<head>
<title>易讯网络建站管理系统--修改管理密码</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<%ajax()%></head>
<body>
<table border="0" cellSpacing="0" cellPadding="0" width="100%" align="center">
<tr><td background="Images/admin_main_01.gif" width="481"></td><td rowspan="2"></td></tr>
<tr><td><img src="Images/admin_main_02.gif" width="481" height="43"></td></tr>
<tr><td><img src="Images/admin_main_03.gif" width="481" height="35"></td>
<td align="right"><img src="Images/admin_main_04.gif" width="68" height="35"></td>
</tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; 修改我的密码</td></tr></table>
<form method="post" action="Admin_ModifyPwd.asp" name="form1" class="required-validate">
<input name="Action" type="hidden" id="Action" value="Modify">
<table border="0" cellspacing="1" cellpadding="0" bgcolor="#E0EBFD" width="100%" class="tabBgColor">
<tr>
<td class="tabheadfont" colspan="2">修改我的密码</td></tr>
<tr>
<td width="100" align="right" class="tdbgFont">用 户 名:</td>
<td class="tdbg"><%=rs("UserName")%></td></tr>
<tr><td width="100" align="right" class="tdbgFont">用户权限:</td><td class="tdbg"><%
select case rs("purview")
	case 1
response.write "超级管理员"
	case 2
response.write "普通管理员"
	case 3
response.write "信息录入员"
	end select
%>
</td>
</tr>
<tr><td width="100" align="right" class="tdbgFont">新 密 码:</td><td class="tdbg"><input id="PwdConfirm" type="password" name="Password" class="input_text required"></td></tr>
<tr><td width="100" align="right" class="tdbgFont">确认密码:</td><td class="tdbg"><input id="Password"  type="password" name="PwdConfirm" class="input_text required validate-equals-Password"></td>
</tr><tr> 
<td class="tdbg"></td>
<td class="tdbg">
<input  type="submit" name="Submit1" value=" 确 定" class="button02-out"> 
<input type="reset" value="复位" name="Submit" class="button02-out">
<input name="Cancel" type="button" id="Cancel" value="取 消" class="button02-out" onClick="window.location.href='Admin_Main.asp'"></td>
</tr>
</table>
</form>
</body>
</html><%
end sub
%>