﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
'禁止代理服务器访问开始,如需要允许访问，请屏蔽此段代码
Rem
  If Request.ServerVariables("HTTP_X_FORWARDED_FOR")  <> "" Then
   Response.Status = "302 Object Moved" 
  End If
Rem
'强制浏览器重新访问服务器下载页面，而不是从缓存读取页面
Response.Buffer = True
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 1
Response.Expires = 0
Response.CacheControl = "no-cache"
%>
<!--#include file="Config/Version.asp"-->
<!--#include file="Config/Config.asp"-->
<html>
<head>
<meta http-equiv="Content-Language" content="zh-cn">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>管理登录</title>
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<script type="text/javascript" language="javascript">
	window.status="建议采用IE5.5以上版本浏览器、1024*768分辨率进入后台！";
</SCRIPT>
<style type="text/css">
<!--
/* =====用户和密码===== */
.input01 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 20px;
	color: #000000;
	text-decoration: none;
	height: 20px;
	width: 130px;
	border: 1px solid #04174A;
	background-image: url(Images/Input.jpg);
	background-repeat: repeat-x;
}
/* =====验证码===== */
.input02 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 20px;
	color: #000000;
	text-decoration: none;
	height: 20px;
	width: 50px;
	border: 1px solid #04174A;
	background-image: url(Images/Input.jpg);
	background-repeat: repeat-x;
}
/* =====提交按钮===== */
.button01-over {
	width: 63px;
	height: 23px;
	background-image: url(Images/Over.gif);
	border: none;
	padding-top: 3px;
}
.button01-out {
	width: 63px;
	height: 23px;
	background-image: url(Images/Normal.gif);
	border: none;
	padding-top: 0px !important;
	padding-top: 3px;
}
.imgcode{ border:1px dotted #FFFFFF;}
-->
</style>
<script type="text/javascript" language=javascript> 
<!--
function CheckBrowser() {
  var app=navigator.appName;
  var verStr=navigator.appVersion;
  if(app.indexOf('Netscape') != -1) {
    alert('系统友情提示：\n    你使用的是Netscape、Firefox或者其他非IE浏览器，可能会导致无法使用后台的部分功能。建议您使用 IE6.0 或以上版本。');
  } else if(app.indexOf('Microsoft') != -1) {
    if (verStr.indexOf('MSIE 3.0')!=-1 || verStr.indexOf('MSIE 4.0') != -1 || verStr.indexOf('MSIE 5.0') != -1 || verStr.indexOf('MSIE 5.1') != -1)
      alert('系统友情提示：\n    您的浏览器版本太低，可能会导致无法使用后台的部分功能。建议您使用 IE6.0 或以上版本。');
  }
}
function refreshimg(){document.all.checkcode.src='Config/ImgCode.asp?'+Math.random();}
//-->
</script>
<script type="text/javascript" language=javascript> 
function SetFocus()
{
if (document.Login.UserName.value=="")
document.Login.UserName.focus();
else
document.Login.UserName.select();
}
function CheckForm()
{
if(document.Login.UserName.value=="")
{
alert("请输入用户名！");
document.Login.UserName.focus();
return false;
}
if(document.Login.Password.value == "")
{
alert("请输入密码！");
document.Login.Password.focus();
return false;
}
if (document.Login.CheckCode.value==""){
alert ("请输入您的验证码！");
document.Login.CheckCode.focus();
return(false);
}
<%
if EnableSiteManageCode = True Then
%>if (document.Login.AdminLoginCode.value==""){
alert ("请输入后台管理认证码！");
document.Login.AdminLoginCode.focus();
return(false);
}
<%
End If
%>}
</script>
</head>
<body style="background-image: url('Images/LoginBG.jpg')">
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">
<tr>
<td height="455">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="510" height="300" background="Images/Login.jpg">
<tr>
<td valign="top" height="113" width="510" colspan="2"></td>
</tr>
<tr>
<td width="230"></td>
<td width="280">
<table border="0" cellpadding="0" cellspacing="0" height="120" width="280">
<form name="Login" action="Admin_ChkLogin.asp" method="post" target="_parent" onSubmit="return CheckForm();">
<tr>
<td width="66" align="right"><font color="#FFFFFF">用户名：</font></td>
<td width="214" colspan="2">
<input type="text" name="UserName" class="input01" value="<%=Request.Cookies("ESYSLoginName")("AutoRemberLoginName")%>" tabindex="1"><input type="checkbox" id="AutoRemberLoginName" name="AutoRemberLoginName" value="1" <% If Request.Cookies("ESYSLoginName")("AutoRemberLoginName")="1" Then Response.Write "checked" End If%>><font color="#FFFFFF">记忆</font>
</td>
</tr>
<tr>
<td width="66" align="right"><font color="#FFFFFF">密&nbsp;&nbsp;码：</font></td>
<td width="214" colspan="2"><input type="password" name="Password" class="input01" tabindex="2"></td>
</tr>
<%if EnableSiteManageCode = True then
Response.write"<tr>"
Response.write"<td align=""right""><font color=""#FFFFFF"">认证码：</font></td><td colspan=""2"">"
Response.write"<input name='AdminLoginCode' size='9' class=""input01"" type=""text""  tabindex=""3"">"
Response.write"</td>"
Response.write"</tr>"
End If%>
<tr>
<td width="66" align="right"><font color="#FFFFFF">验证码：</font></td>
<td width="56"><input name="CheckCode" maxlength="6" class="input02"  tabindex="4"></td>
<td width="158"><a href='javascript:refreshimg()' title='看不清楚，换个图片'>
<img id='checkcode' src="Config/ImgCode.asp" align="middle" border="0" class="imgcode"></a></td>
</tr>
<tr>
<td width="66" align="right">　</td>
<td width="214" colspan="2">
<input name="Submit1" type="submit" class="button01-out"  value="&#30830;&#23450;"  tabindex="5">
<input name="Submit2" type="reset" class="button01-out" value="&#37325;&#32622;"  tabindex="6"></td>
</tr>
</form>
</table>
</td>
</tr>
<tr>
<td valign="top" height="66" width="510" colspan="2"></td>
</tr>
</table></td>
</tr>
<tr>
<td></td>
</tr>
</table>
<script language='JavaScript' type='text/JavaScript'> 
CheckBrowser();
SetFocus();
</script>
</body>
</html>