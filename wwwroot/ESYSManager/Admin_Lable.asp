﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=1    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/ClassList_Fun.asp"-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Version" content="<%=SystemVer%>">
<title>自由标签管理</title>
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<script src="Config/Windows.js" type="text/javascript"></script>
<%Ajax()%></head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; <a href="Admin_Lable.asp">动态嵌套标签</a> &gt; <font color="#FF0000"><%
Select Case Request("ProdId")
Case 1
	Response.write " 单页标签设置"
Case 2
	Response.write " 常规标签设置"
Case 3
	Response.write " 产品标签设置"
Case 4   
	Response.write " 下载标签设置"
Case Else
End Select
%></font></td></tr></table><%
Dim Action,ParentID,i,FoundErr,ErrMsg
Action=trim(request("Action"))
if Action="Add" then
	call AddCentont()
elseif Action="SaveAdd" then
	call SaveAdd()
elseif Action="Modify" then
	call Modify()
elseif Action="SaveModify" then
	call SaveModify()
elseif Action="Del" then
	call DelLable()
else
	call main()
end if
if FoundErr=True then
	call WriteErrMsg(ErrMsg)
end if
call CloseConn()
Sub main()  '标签列表
Dim sKey
    sKey=jencode(Replace(Request("sKey"),"'",""))
Dim Special,SpecialPage
	Special=Request("Special")
	SpecialPage=Request("Special")
	If Special<>"" Then
        Special="and Special="&Special&""
    End If
Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
Dim Sql
    Sql="Select * From Easyes_Label Where AspHtml=1 and Title Like '%"&sKey&"%' "&Special&" Order By Id DESC"
    Rs.PageSize=20
	Rs.CacheSize=Rs.PageSize
    Rs.Open Sql,Conn,1,1
Dim CurrentPage
    If Request("CurrentPage")="" Then
        CurrentPage=1
    Else
        CurrentPage=Request("CurrentPage")
    End If    
    If Not(Rs.Eof And Rs.Bof) Then
        Rs.AbsolutePage=CurrentPage
    End If
%><input name="Submit32" type="button" class="button01-out" value="+单页标签" onClick="window.location='Admin_Lable.asp?Action=Add&Special=1'" title="在当前位置添加新标签">
<input name="Submit32" type="button" class="button01-out" value="+常规标签" onClick="window.location='Admin_Lable.asp?Action=Add&Special=2'" title="在当前位置添加新标签">
<input name="Submit32" type="button" class="button01-out" value="+产品标签" onClick="window.location='Admin_Lable.asp?Action=Add&Special=3'" title="在当前位置添加新标签">
<input name="Submit32" type="button" class="button01-out" value="+下载标签" onClick="window.location='Admin_Lable.asp?Action=Add&Special=4'" title="在当前位置添加新标签">
<table border="0" cellpadding="2" cellspacing="1" class="tabBgColor" width="100%">
<tr> 
<td height="25" class="tdbg" colspan="6"><a href="?Special=1">单页模块</a> | <a href="?Special=2">常规模块</a> | <a href="?Special=3">产品模块</a> | <a href="?Special=4">下载模块</a>
</td>
</tr>
<tr> 
<td height="25" align="center" class="tabheadfont" width="40">ID</td>
<td height="25" align="center" class="tabheadfont" width="100">资源所属模块</td>
<td height="25" align="center" class="tabheadfont">动态标签标题</td>
<td height="22" align="center" class="tabheadfont" width="120">更新时间</td>
<td height="22" align="center" class="tabheadfont" width="120">添加时间</td>
<td width="80" height="22" align="center" class="tabheadfont">常规操作</td>
</tr>
<%
  Dim I
  For I=1 To Rs.PageSize
    If Rs.Eof Then
        Exit For
    End If
%><tr class="tdbg">
<td align="center" width="40"><%=rs("ID")%></td>
<td align="center"><%Call Channel(rs("Special"))%></td>
<td><img border="0" src="Images/arrow_r.gif"><%Response.write "&nbsp;<a href='Admin_Lable.asp?Action=Modify&ID=" & rs("ID") &"&Special=" & rs("Special") &"'>"&juncode(Rs("Title"))&"</a>"%></td>
<td align="center"><%=Rs("upTime")%></td>
<td align="center"><%=Rs("AddTime")%></td>
<td align="center"><%
response.write "<a href='Admin_Lable.asp?Action=Modify&ID=" & rs("ID") &"&Special=" & rs("Special") &"'>编辑</a>&nbsp;"
response.write "<a href='Admin_Lable.asp?Action=Del&ID=" & rs("ID") & "' onClick=""return confirm('确定要删除吗？');"">删除</a>&nbsp;"
%></td>
</tr><%
  Rs.MoveNext
  Next
%></table> 
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr> 
<td align="right" bgcolor="#f6f6f6">
<script type="text/javascript" src="Config/PageList.js"></script>
<script type="text/javascript" language="JavaScript">
	<!--
	var plb = new PageListBar('plb',<%=Rs.PageCount%>,<%=CurrentPage%>,'&sKey=<%=sKey%>&Special=<%=SpecialPage%>',20);
	document.write(plb);
	//-->
</script>
</td>
</tr>
</table>
<form name="form1" method="post" action="Admin_Lable.asp?">
<table width="100%" border="0" cellpadding="3" cellspacing="1" class="tabBgColor">
<tr><td width="67%" align="right" class="tdbgfont">搜索<u>自定义标签标题</u>: </td>
<td width="25%" align="right" class="tdBg"><input name="sKey" type="text" class="input_text" id="sKey" style="width:100%" value="<%=Trim(Request("sKey"))%>"></td>
<td width="5%" align="center" class="tdBg"><input name="SearchButton" type="submit" class="button02-out" value="确  定"></td>
</tr>
</table>
</form><%
Rs.Close
Set Rs=Nothing
End sub
Sub AddCentont
Dim Special
	Special=Request("Special")
%><form name="myform" method="post" action="Admin_Lable.asp?Work=SaveAddReco" class="required-validate">
<input name="Special" type="hidden" ID="Special" value="<%=Special%>">
<table width="100%" border="0" cellpadding="2" cellspacing="1" class="tabBgColor">
<tr> 
<td colspan="2" align="center" class="tabheadfont">添加自定义标签库</td>
</tr>
<tr>
<td align="right" class="tdbgfont">标签标题:</td>
<td class="tdbg">
<input name="Title" type="text" size="45" class="input_text required"></td>
</tr>
<tr>
<td align="right" class="tdbgfont" rowspan="2">标签控制:</td>
<td class="tdbg">
<%
	Response.write "<div><div style=""float: left""><img border=""0"" src=""Images/Lable1.gif""></div>"
	Response.write "<div style=""float: left"">"
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=Class&LabelID=0&Special="&Special&""",400,350)>栏目标签</a> | "
If Request("Special")=1 Then
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=Apage&LabelID=0&Special=1"",400,230)>内容标签</a>"
Else
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=News1&LabelID=0&Special="&Special&""",400,400)>版块标签</a> | "
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=News2&LabelID=0&Special="&Special&""",400,400)>翻页标签</a> | "
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=News3&LabelID=0&Special="&Special&""",400,400)>内容标签</a> "
End If
	Response.write "</div></div>"
%>
</td>
</tr>
<tr>
<td class="tdbg">
<textarea rows="5" name="StyleLable" cols="100" class="input_text required"></textarea></td>
</tr>
<tr>
<td align="right" class="tdbgfont" rowspan="2">样式定义:</td>
<td class="tdbg">
<%
	Response.write "<div><div style=""float: left""><img border=""0"" src=""Images/Lable2.gif""></div>"
	Response.write "<div style=""float: left"">"
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=Lable&Special="&Special&""",380,360)>系统标签库查看</a>"
	Response.write "</div></div>"
%></td>
</tr>
<tr>
<td class="tdbg">
<textarea rows="20" name="Content" cols="100" class="input_text required"></textarea></td>
</tr>
<tr class="tdbg"> 
<td align="right">
<input name="Action" type="hidden" id="Action" value="SaveAdd"></td>
<td>
<input name="Submit1" type="submit" class="button02-out" value=" 保 存 ">
<input name="Submit2" type="reset" class="button02-out" value=" 还 原 ">
<input name="Submit3" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Lable.asp'"></td>
</tr>
</table>
</form><%
End Sub
sub Modify()
Dim ID
	ID=trim(request("ID"))
Dim Special
	Special=Request("Special")
	if ID="" then
		FoundErr=True
		ErrMsg=ErrMsg & "·请指定要修改自定义标签！\n"
		exit sub
	end if
Dim Rs
    Set Rs=Conn.ExeCute("Select * From Easyes_Label Where Id="&ID)
    If Rs.Eof And Rs.Bof Then
        Rs.Close
        Set Rs=Nothing
        Response.Write("记录未找到")
        Response.End
    End If
    Dim Title,Content,StyleLable
    Id=Rs("Id")
    Title=Rs("Title")
    StyleLable=Rs("StyleLable")
    Content=Rs("Content")
    Rs.Close
    Set Rs=Nothing
%><form name="form2" method="post" action="Admin_Lable.asp?Work=SaveAddReco" class="required-validate">
<input name="ID" type="hidden" id="ID" value="<%=ID%>">
<input name="Action" type="hidden" id="Action" value="SaveModify">
<table width="100%" border="0" cellpadding="2" cellspacing="1" class="tabBgColor">
<tr> 
<td colspan="2" align="center" class="tabheadfont">编辑自定义标签库</td>
</tr>
<tr> 
<td align="right" class="tdbgfont">标签标题:</td>
<td class="tdbg">
<input name="Title" type="text" class="input_text required" size="45" value="<%=juncode(Server.HTMLEncode(Title))%>" ></td>
</tr>
<tr> 
<td align="right" class="tdbgfont" rowspan="2">标签控制:</td>
<td class="tdbg">
<%
	Response.write "<div><div style=""float: left""><img border=""0"" src=""Images/Lable1.gif""></div>"
	Response.write "<div style=""float: left"">"
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=Class&LabelID="&ID&"&Special="&Special&""",400,350)>栏目标签</a> | "
If Request("Special")=1 Then
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=Apage&LabelID="&ID&"&Special=1"",400,230)>内容标签</a>"
Else
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=News1&LabelID="&ID&"&Special="&Special&""",400,400)>版块标签</a> | "
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=News2&LabelID="&ID&"&Special="&Special&""",400,400)>翻页标签</a> | "
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=News3&LabelID="&ID&"&Special="&Special&""",400,400)>内容标签</a> "
End If
	Response.write "</div></div>"
%>
</td>
</tr>
<tr> 
<td class="tdbg">
<textarea rows="5" name="StyleLable" cols="100" class="input_text required"><%=Server.HTMLEncode(StyleLable)%></textarea></td>
</tr>
<tr> 
<td align="right" class="tdbgfont" rowspan="2">样式定义:</td>
<td class="tdbg">
<%
	Response.write "<div><div style=""float: left""><img border=""0"" src=""Images/Lable2.gif""></div>"
	Response.write "<div style=""float: left"">"
	Response.write "<a href=javascript:openwin2(""admin_labelStyle.asp?action=Lable&Special="&Special&""",380,360)>系统标签库查看</a>"
	Response.write "</div></div>"
%></td>
</tr>
<tr> 
<td class="tdbg">
<textarea rows="20" name="Content" cols="100" class="input_text required"><%=Server.HTMLEncode(Content)%></textarea></td>
</tr>
<tr class="tdbg"> 
<td align="right"></td>
<td>
<input name="Submit1" type="submit" class="button02-out" value=" 保 存 ">
<input name="Submit2" type="reset" class="button02-out" value=" 还 原 ">
<input name="Submit3" type="reset" class="button02-out" value="返 回" onClick="window.location.href='Admin_Lable.asp'">
</td>
</tr>
</table>
</form>
<%
End Sub
%>
</body>
</html><%
'添加
Sub SaveAdd()
    Dim Title
Title=Trim(Request("Title"))
    Dim Sql
        Sql="Select Top 1 * From Easyes_Label Where Title='" & Title & "'"
    Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
        Rs.Open Sql,Conn,1,3
If Not(rs.bof and rs.eof) then
	Call WriteErrMsg("·标签名称已经存在！请换一个再试试！")
Else
    Rs.AddNew
    Rs("Special")= Trim(Request("Special")) 
    Rs("Title")= jencode(Trim(Request("Title")))
    Rs("StyleLable")= Trim(Request("StyleLable"))
    Rs("Content")= Trim(Request("Content"))
    Rs("AspHtml")= 1
    Rs("AddTime")= Now
    Rs("upTime")= Now
    Rs.Update
Dim Special
    	Special=Rs("Special")
    Rs.Close
    Set Rs=Nothing
End If
    Response.Redirect("Admin_Lable.asp?Special="&Special&"")
End sub

'修改
sub SaveModify()
Dim ID
    ID= Trim(Request("ID"))
    Dim Sql
        Sql="Select * From Easyes_Label Where ID=" & ID
    Dim Rs
    Set Rs=Server.CreateObject("ADODB.RecordSet")
        Rs.Open Sql,Conn,1,3
	If Rs.Eof And Rs.Bof Then
		FoundErr=True
		ErrMsg=ErrMsg & "·找不到自定义标签的内容！\n"
		rs.close
		set rs=Nothing
    	Exit sub
	End if
    Rs("Title")= jencode(Trim(Request("Title")))
    Rs("StyleLable")= Trim(Request("StyleLable"))
    Rs("Content")= Trim(Request("Content"))
    Rs("upTime")= Now
    Rs.Update
Dim Special
    	Special=Rs("Special")
    Rs.Close
    Set Rs=Nothing
    Response.Redirect("Admin_Lable.asp?Special="&Special&"")
End sub

Sub DelLable() '删除
Dim FoundErr,ErrMsg
	if session("purview")>1 then
		Call WriteErrMsg("·没有操作权限！\n")
	end if
Dim ComeUrl
	ComeUrl=Request.ServerVariables("HTTP_REFERER")
Dim ID
	ID=trim(request("ID"))
	If ID="" then
		Call WriteErrMsg("·请先选定标签！\n")
	End if
Dim Sql
	Sql="Delete From [Easyes_Label] Where AspHtml=1 and ID=" &ID
	Conn.ExeCute(Sql)
	Call CloseConn()      
	response.redirect ComeUrl
End Sub
%>