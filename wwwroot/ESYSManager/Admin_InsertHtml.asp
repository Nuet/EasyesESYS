﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=2    '操作权限 1超级用户2高级管理员3信息录入员
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"-->
<!--#include file="Config/Class.FSO.asp"-->
<!--#include file="Config/ClassList_Fun.asp"-->
<!--#include file="Label/TemplateClass.asp"-->
<!--#include file="Label/Label.asp"-->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Version" content="<%=SystemVer%>">
<title>执行静态标签</title>
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; <a href="Admin_InsertHtml.asp">静态嵌套标签</a></td></tr></table><%
Dim Action,ParentID,i,FoundErr,ErrMsg
Action=trim(request("Action"))
if Action="DoInsertSysActive" then
	call DoInsertSysActive()	
else
	call main()
end if
if FoundErr=True then
	call WriteErrMsg(ErrMsg)
end if
call CloseConn()
Sub main()  '标签列表
%><form name="form3" method="post" action="Admin_InsertHtml.asp?Action=DoInsertSysActive">
<table width="100%" border="0" cellpadding="2" cellspacing="1" class="tabBgColor" align="center">
<tr><td height="21" align="center" class="tabheadfont">执行页面内容替换命令(请使用Ctrl\Shift组合键)</td></tr>
<tr><td align="center" valign="top" bgcolor="#FFFFFF">
<select name="RecordList" size="25" multiple class="input_text" style="width:100%;height:450px;">
<%
    Dim Sql
        Sql="Select ID,Title,UpTime From [Easyes_Label] Where AspHtml=2 Order By ID DESC"
    Dim Rs
    Set Rs=Conn.ExeCute(Sql)
    While Not Rs.Eof
%><option value="<%=Rs("Id")%>">·<%=Rs("Title")%></option>
<%
    Rs.MoveNext
    Wend
    Rs.Close
    Set Rs=Nothing
%></select></td></tr>
<tr><td align="center" valign="top" bgcolor="#FFFFFF">
<input name="Submit5" type="submit" class="button01-out" value="确  定">
<input name="Submit23" type="reset" class="button01-out" value="还  原">
</td>
</tr>
</table>
</form>
<%
End Sub
%></body>
</html><%
'添加
Sub DoInsertSysActive() '执行替换
    Dim RecordList
        RecordList=Trim(Replace(Request("RecordList")," ",""))
    If RecordList="" Then
		call WriteErrMsg("·请您选择要执行的[内容替换命令]！\n")
    End If
    Dim arrRecordList
        arrRecordList=Split(RecordList,",",-1,1)
    Dim Sql
        Sql=""
    Dim Rs
    Dim arrFileList
    Dim TClass
    Set TClass=New ESYS_TemplateClass
    Dim I,J
    For I=0 To UBound(arrRecordList)
        Sql="Select * From [Easyes_Label] Where AspHtml=2 and ID=" & arrRecordList(I)
        Set Rs=Conn.ExeCute(Sql)
        If Not(Rs.Eof And Rs.Bof) Then
            arrFileList=Split(Rs("FileList"),vbCrLf,-1)
            For J=0 To UBound(arrFileList)
                With TClass
                    .OpenTemplate(Server.MapPath(arrFileList(J)))
                    .StartElement=Rs("StartElement")
                    .EndElement=Rs("EndElement")
'                    Response.write TemplateTags(Rs("StyleLable"))  '测试专用
'                    Response.end
                    .Value=TemplateTags(Rs("StyleLable"))
                    .ReplaceTemplate()
                    .Save()
                End With
            Next
        End If
        Rs.Close
    Next
    Set TClass=Nothing
    Set Rs=Nothing
		Call WriteSuccessMsg("·内容替换完毕！\n")
End Sub

'//////////////////////////////////////
'//函数：取得样式表标签信息
'//参数：样式表标签的ID
'//返回：标签内容
Function GetLableTemplate(ID)
	Dim Sql,Rs2
	GetLableTemplate=""
	Sql="SELECT * From [Easyes_Label] Where AspHtml=2 and ID="&ID
	Set Rs2=Server.CreateObject("ADODB.RecordSet")
	Rs2.Open Sql,Conn,1,3
	If Not(Rs2.Eof And Rs2.Bof) Then
		GetLableTemplate=Rs2("Content")
		Session("buffer_Template")=GetLableTemplate
	End If
	Rs2.Close
	Set Rs2=Nothing
End Function
%>