﻿<%
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************

'【功能】处理自定义模板标签
Function TemplateTags(ByVal sContent)
         Dim objRegEx, Match, Matches
      '建立正则表达式
         Set objRegEx = New RegExp
      '查找内容
         objRegEx.Pattern = "<Lable:[^<>]+?\/>"
      '忽略大小写
         objRegEx.IgnoreCase = True
      '全局查找
         objRegEx.Global = True
         Set Matches = objRegEx.Execute(sContent)
      '循环已发现的匹配:ParseTag[符合标签语法的意思]
         For Each Match in Matches
         sContent = Replace(sContent, Match.Value,ParseTag(Match.Value))
         Next
      '消毁对象
         set Matches = nothing
         set objRegEx = nothing
      '返回值
         TemplateTags = sContent
End Function

'【功能】取得模板标签的参数名
'如：<Lable:loop channelid="1" pagesize="10" title="20" type="NEW" column="1">
Function GetAttribute(ByVal strAttribute,ByVal strTag)
      Dim objRegEx, Matches
      '建立正则表达式
         Set objRegEx = New RegExp
      '查找内容
         objRegEx.Pattern = lCase(strAttribute) & "=""[0-9a-zA-Z{-}/\s=()]*"""
      '忽略大小写
         objRegEx.IgnoreCase = True
      '全局查找
         objRegEx.Global = True
      '执行搜索
         Set Matches = objRegEx.Execute(strTag)
      '如有匹配的则返回值, 不然返回空值
         If Matches.Count > 0 then
              GetAttribute = Split(Matches(0).Value,"""")(1)
         Else
              GetAttribute = ""
         End if
      '消毁对象
         Set Matches = nothing
         Set objRegEx = nothing
End function

'【功能】转换标签中HTML“[]”为“<>”
'【LableEncode】：标签中特殊字符的转换
'***************************************************
Function LableEncode(str)
If not isnull(str) Then
	str = Replace(Str, "{", "<")
	str = Replace(Str, "}", ">")
	str = Replace(Str, "(", """")
	str = Replace(Str, ")", """")
    LableEncode = str
End if
End function

'【功能】解析并替换相应的模板标签内容
'【ParseTag】标签语法的意思
Function ParseTag(ByVal strTag)
   Dim arrResult,ClassName,arrAttributes,sTemp,i,objClass
      '如果标签是空的则退出函数
       if len(strTag) = 0 then exit Function
		arrResult = Split(strTag,":")
		ClassName = Split(arrResult(1)," ")(0)
   Select case uCase(ClassName)
'//★◆★◆★◆★◆★◆★◆★◆★◆★◆★◆
'//栏目导航
'//#################################################
      Case "CLASS_LABLE"   '栏目资源--栏目导航
         set objClass = new TemplateSkin_Class
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         objClass.ESParent= GetAttribute("ESParent",strTag)
         objClass.ESLock= GetAttribute("ESLock",strTag)
         objClass.ESNList= GetAttribute("ESNList",strTag)
         objClass.ESulS= GetAttribute("ESulS",strTag)
         objClass.ESulE= GetAttribute("ESulE",strTag)
         objClass.ESliS= GetAttribute("ESliS",strTag)
         objClass.ESliE= GetAttribute("ESliE",strTag)
         objClass.OrderField= GetAttribute("OrderField",strTag)
         objClass.OrderType= GetAttribute("OrderType",strTag)
         ParseTag =objClass.Easyes_Class(GetAttribute("Easyes_Class",StrTag))
         Set objClass = nothing
'//★◆★◆★◆★◆★◆★◆★◆★◆★◆★◆
'//单页资源
'//#################################################
      Case "APAGE_LABLE"  '单页资源--内容查看
         set objClass = new TemplateSkin_Content
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         objClass.ESLock= GetAttribute("ESLock",strTag)
         ParseTag =objClass.Easyes_ESApage(GetAttribute("Easyes_ESApage",StrTag))
         Set objClass = nothing
'//★◆★◆★◆★◆★◆★◆★◆★◆★◆★◆
'//常规资源
'//#################################################
      Case "NEWS_LABLE"   '常规资源--列表没有分页
         set objClass = new TemplateSkin_News
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         objClass.ESParent= GetAttribute("ESParent",strTag)
         objClass.ESLock= GetAttribute("ESLock",strTag)
         objClass.ESSpecial= GetAttribute("ESSpecial",strTag)
         objClass.ESNPage= GetAttribute("ESNPage",strTag)
         objClass.ESNList= GetAttribute("ESNList",strTag)
         objClass.ESulS= GetAttribute("ESulS",strTag)
         objClass.ESulE= GetAttribute("ESulE",strTag)
         objClass.ESliS= GetAttribute("ESliS",strTag)
         objClass.ESliE= GetAttribute("ESliE",strTag)
         objClass.OrderField= GetAttribute("OrderField",strTag)
         objClass.OrderType= GetAttribute("OrderType",strTag)
         ParseTag =objClass.Easyes_NewsList(GetAttribute("Easyes_NewsList",StrTag))
         Set objClass = nothing
'//#################################################
      Case "NEWS_NLIST"   '常规资源--列表有分页
         set objClass = new TemplateSkin_NewsN
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         objClass.ESParent= GetAttribute("ESParent",strTag)
         objClass.ESLock= GetAttribute("ESLock",strTag)
         objClass.ESSpecial= GetAttribute("ESSpecial",strTag)
         objClass.ESSearch= GetAttribute("ESSearch",strTag)
         objClass.ESNPage= GetAttribute("ESNPage",strTag)
         objClass.ESNList= GetAttribute("ESNList",strTag)
         objClass.ESulS= GetAttribute("ESulS",strTag)
         objClass.ESulE= GetAttribute("ESulE",strTag)
         objClass.ESliS= GetAttribute("ESliS",strTag)
         objClass.ESliE= GetAttribute("ESliE",strTag)
         objClass.ESPageS= GetAttribute("ESPageS",strTag)
         objClass.ESPageE= GetAttribute("ESPageE",strTag)
         objClass.OrderField= GetAttribute("OrderField",strTag)
         objClass.OrderType= GetAttribute("OrderType",strTag)
         ParseTag =objClass.Easyes_NewsNList(GetAttribute("Easyes_NewsNList",StrTag))
         Set objClass = nothing
'//#################################################
      Case "NEWS_PAGE"   '常规资源--内容查看
         set objClass = new TemplateSkin_NewsPage
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         ParseTag =objClass.Easyes_NewsPage(GetAttribute("Easyes_NewsPage",StrTag))
         Set objClass = nothing
'//★◆★◆★◆★◆★◆★◆★◆★◆★◆★◆
'//产品资源
'//#################################################
      Case "PROD_LABLE"   '常规资源--列表没有分页
         set objClass = new TemplateSkin_Prod
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         objClass.ESParent= GetAttribute("ESParent",strTag)
         objClass.ESLock= GetAttribute("ESLock",strTag)
         objClass.ESSpecial= GetAttribute("ESSpecial",strTag)
         objClass.ESNPage= GetAttribute("ESNPage",strTag)
         objClass.ESNList= GetAttribute("ESNList",strTag)
         objClass.ESulS= GetAttribute("ESulS",strTag)
         objClass.ESulE= GetAttribute("ESulE",strTag)
         objClass.ESliS= GetAttribute("ESliS",strTag)
         objClass.ESliE= GetAttribute("ESliE",strTag)
         objClass.OrderField= GetAttribute("OrderField",strTag)
         objClass.OrderType= GetAttribute("OrderType",strTag)
         ParseTag =objClass.Easyes_ProdList(GetAttribute("Easyes_ProdList",StrTag))
         Set objClass = nothing
'//#################################################
      Case "PROD_NLIST"   '产品资源--列表有分页
         set objClass = new TemplateSkin_ProdN
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         objClass.ESParent= GetAttribute("ESParent",strTag)
         objClass.ESLock= GetAttribute("ESLock",strTag)
         objClass.ESSpecial= GetAttribute("ESSpecial",strTag)
         objClass.ESSearch= GetAttribute("ESSearch",strTag)
         objClass.ESNPage= GetAttribute("ESNPage",strTag)
         objClass.ESNList= GetAttribute("ESNList",strTag)
         objClass.ESulS= GetAttribute("ESulS",strTag)
         objClass.ESulE= GetAttribute("ESulE",strTag)
         objClass.ESliS= GetAttribute("ESliS",strTag)
         objClass.ESliE= GetAttribute("ESliE",strTag)
         objClass.ESPageS= GetAttribute("ESPageS",strTag)
         objClass.ESPageE= GetAttribute("ESPageE",strTag)
         objClass.OrderField= GetAttribute("OrderField",strTag)
         objClass.OrderType= GetAttribute("OrderType",strTag)
         ParseTag =objClass.Easyes_ProdNList(GetAttribute("Easyes_ProdNList",StrTag))
         Set objClass = nothing
'//#################################################
      Case "PROD_PAGE"   '产品资源--内容查看
         set objClass = new TemplateSkin_ProdPage
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         ParseTag =objClass.Easyes_ProdPage(GetAttribute("Easyes_ProdPage",StrTag))
         Set objClass = nothing
'//★◆★◆★◆★◆★◆★◆★◆★◆★◆★◆
'//下载资源
'//#################################################
      Case "DOWN_LABLE"   '下载资源--列表没有分页
         set objClass = new TemplateSkin_Down
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         objClass.ESParent= GetAttribute("ESParent",strTag)
         objClass.ESLock= GetAttribute("ESLock",strTag)
         objClass.ESSpecial= GetAttribute("ESSpecial",strTag)
         objClass.ESNPage= GetAttribute("ESNPage",strTag)
         objClass.ESNList= GetAttribute("ESNList",strTag)
         objClass.ESulS= GetAttribute("ESulS",strTag)
         objClass.ESulE= GetAttribute("ESulE",strTag)
         objClass.ESliS= GetAttribute("ESliS",strTag)
         objClass.ESliE= GetAttribute("ESliE",strTag)
         objClass.OrderField= GetAttribute("OrderField",strTag)
         objClass.OrderType= GetAttribute("OrderType",strTag)
         ParseTag =objClass.Easyes_DownList(GetAttribute("Easyes_DownList",StrTag))
         Set objClass = nothing
'//#################################################
      Case "DOWN_NLIST"   '下载资源--列表有分页
         set objClass = new TemplateSkin_DownN
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         objClass.ESParent= GetAttribute("ESParent",strTag)
         objClass.ESLock= GetAttribute("ESLock",strTag)
         objClass.ESSpecial= GetAttribute("ESSpecial",strTag)
         objClass.ESSearch= GetAttribute("ESSearch",strTag)
         objClass.ESNPage= GetAttribute("ESNPage",strTag)
         objClass.ESNList= GetAttribute("ESNList",strTag)
         objClass.ESulS= GetAttribute("ESulS",strTag)
         objClass.ESulE= GetAttribute("ESulE",strTag)
         objClass.ESliS= GetAttribute("ESliS",strTag)
         objClass.ESliE= GetAttribute("ESliE",strTag)
         objClass.ESPageS= GetAttribute("ESPageS",strTag)
         objClass.ESPageE= GetAttribute("ESPageE",strTag)
         objClass.OrderField= GetAttribute("OrderField",strTag)
         objClass.OrderType= GetAttribute("OrderType",strTag)
         ParseTag =objClass.Easyes_DownNList(GetAttribute("Easyes_DownNList",StrTag))
         Set objClass = nothing
'//#################################################
      Case "DOWN_PAGE"   '下载资源--内容查看
         set objClass = new TemplateSkin_DownPage
         objClass.ESLable= GetAttribute("ESLable",strTag)
         objClass.ESClass= GetAttribute("ESClass",strTag)
         ParseTag =objClass.Easyes_DownPage(GetAttribute("Easyes_DownPage",StrTag))
         Set objClass = nothing
      End select
End function
%>