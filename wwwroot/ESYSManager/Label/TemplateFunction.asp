﻿<%
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************

'//////////////////////////////////////
'//函数：取得样式表标签信息
'//参数：样式表标签的ID
'//返回：标签内容
Function GetLableTemplate(ID)
	Dim Sql,Rs2
	GetLableTemplate=""
	Sql="SELECT * From [Easyes_Label] Where ID="&ID
	Set Rs2=Server.CreateObject("ADODB.RecordSet")
	Rs2.Open Sql,Conn,1,3
	If Not(Rs2.Eof And Rs2.Bof) Then
		GetLableTemplate=Rs2("Content")
		Session("buffer_Template")=GetLableTemplate
	End If
	Rs2.Close
	Set Rs2=Nothing
End Function

'//////////////////////////////////////
'//函数：取得系统常量的标签
'//参数：样式表标签内容
'//返回：标签内容
Function SystemLable(Template)
'调用类
Dim StrClass
Set StrClass = New ESYS_StringClass
Dim Asp_Lable
	Asp_Lable="{\$SiteName\$}"   '网站名称
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&SiteName)
	Asp_Lable="{\$SiteUrl\$}"   '网站地址
	Template = StrClass.ReplaceTest(Asp_Lable,Template,""&SiteUrl)
End Function

'//函数：带参数的标签获取者
'//参数：DiyLable:标签，Template:包含标签的内容
Function ReplaceTestDiy(DiyLable,Template,Content,TitleFontColor,TitleFontType)
'调用类
Dim StrClass
Set StrClass = New ESYS_StringClass
	ReplaceTestDiy=Template
	Dim regEx,Match,myMatches
	Set regEx=New RegExp
		regEx.Pattern = DiyLable
		regEx.IgnoreCase = True
		regEx.Global = True
	Set myMatches=regEx.Execute(Template)
	If myMatches.Count<=0 Then Exit function
	For Each Match In myMatches
Dim N
	N=CLng(Match.SubMatches.item(0))
		DiyLable=Replace(ReplaceTestDiy,"{$News_TitleHtml:" & N & "$}",TitleSytle(TitleFontColor,TitleFontType,StrClass.CutStr(Content,N)))
	Next
		ReplaceTestDiy=DiyLable
End Function
%>