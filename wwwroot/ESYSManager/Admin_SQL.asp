﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Response.buffer=true	
Const PurviewLevel=1    '操作权限
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"--><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>数据库压缩与备份</title>
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
<%ajax()%></head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; 数据库管理</td></tr></table>
<%
Dim Work
    Work=Request("Work")
Select Case Work
    Case "DoExeCuteSql"
        Call DoExeCuteSql()
    Case Else
        Call CompactDB()
End Select
Sub CompactDB()
%>
<form action="Admin_SQL.asp" method="post" name="form4" class="required-validate">
<table cellspacing="1" width="100%" cellpadding="2" class="tabBgColor">
<tr>
<td class="tabHeadFont">注：一次只能执行一条Sql语句。<font color="#FF0000">此操作非常威胁，如不熟悉SQL语句请慎重操作</font></td> 
</tr><input name="Work" type="hidden" id="Work" value="DoExeCuteSql"> 
<tr>
<td class="tdbgFont"><textarea name="Content" id="Content" class="input_text required" style="width:100%;height:200px"></textarea></td>
</tr>
<tr>
<td align="center" class="tdbgFont">
<input name="Submit4" type="submit" value="确  定" class="button02-out"> 
<input name="Submit22" type="reset" value="还  原" class="button02-out"></td> 
</tr>
</table>
</form>
<%
End Sub
%> 
</body>
</html>
<%
//执行Sql语句操作
Sub DoExeCuteSql()
Dim Content
Content=Request("Content")
Err.Clear
On Error Resume Next
Conn.BeginTrans
Conn.ExeCute(Content)
If Err.Number<>0 Then
	Conn.Rollback
	Call WriteErrMsg("·SQL语句有误,请检查你的SQL语句\n")
Else
	Conn.CommitTrans
	Call WriteSuccessMsg("·SQL语句执行完毕\n")
End If
	Response.End
End Sub
%>