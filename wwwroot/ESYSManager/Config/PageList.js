﻿/*
    title   :       page list bar
    creator :       tklmilk
    date    :       2004-9-29

    example:
        var plb = new PageListBar('plb', 343, 6, '?', 10);
        document.write(plb);
*/

function PageListBar(name, pagecount, CurrentPage, url, listlength)
{
    this.name = name;
    this.pagecount = pagecount;                                 //total page num
    this.CurrentPage = CurrentPage;                                   //current page
    this.url = url;                                             //link
    this.listlength = listlength?listlength:10;                                       //number of page showed

    if(this.pagecount <=0)
    {
        this.pagecount = 1;
    }
    if(this.CurrentPage > this.pagecount)
    {
        this.CurrentPage = this.pagecount;
    }
}

PageListBar.prototype.go = function(pagenum)
{
    window.location.href = '?CurrentPage=' + pagenum + this.url;
}

PageListBar.prototype.goto = function()
{
    var CurrentPage = prompt("请输入跳转到的页号",this.CurrentPage);
    if(CurrentPage)
    {
        if(CurrentPage <= this.pagecount && CurrentPage>=1)
        {
            this.go(CurrentPage);
        }
    }
}

PageListBar.prototype.toString = function()
{
    var str = '', pStart = pEnd = 1;

    if(this.pagecount <= 1)
    {
        pStart = pEnd = 1;
    }else{
        if(this.pagecount <= this.listlength)
        {
            pStart = 1;
            pEnd = this.pagecount;
        }else{
            var movestep = Math.round(this.listlength/2);
            if(this.CurrentPage > movestep)
            {
                pStart = this.CurrentPage - movestep;
                pEnd = this.CurrentPage + movestep;
                if(pEnd > this.pagecount)
                {
                    pStart -= pEnd - this.pagecount;
                    pEnd = this.pagecount;
                }

                if(pEnd > this.pagecount)
                {
                    pEnd = this.pagecount;
                    pStart -= (pEnd - this.pagecount);
                }
            }else{
                pStart = 1;
                pEnd = this.listlength;
            }
        }
    }

    for(var i=pStart; i<=pEnd; i++)
    {
        str += '<a href="javascript:' + this.name + '.go(' + i + ');void(0);">' + (i==this.CurrentPage?('<b>' + i + '</b>'):i) + '</a>&nbsp;';
    }

    str = '共' + this.pagecount + '页&nbsp;&nbsp;<a href="javascript:' + this.name + '.go(1);void(0);">首页</a>&nbsp;<a href="javascript:' + this.name + '.go(' + ((this.CurrentPage-1)<=1?1:(this.CurrentPage-1)) + ');void(0);"' + (this.CurrentPage==1?'disabled':'') + '>&laquo;</a>&nbsp;' + str + '<a href="javascript:' + this.name + '.go(' + ((this.CurrentPage+1)>=this.pagecount?this.pagecount:(this.CurrentPage+1)) + ');void(0);"' + (this.CurrentPage==this.pagecount?'disabled':'') + '>&raquo;</a>&nbsp;<a href="javascript:' + this.name + '.go(' + this.pagecount + ');void(0);">尾页</a>&nbsp;<a href="javascript:' + this.name + '.goto();void(0);">跳转</a>';

    return str;
}