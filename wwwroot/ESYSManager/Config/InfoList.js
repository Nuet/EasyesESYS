﻿<!--
function selectAllCheckBox(obj)
{
	if(obj.length)
	{
		for(var i=0;i<obj.length;i++)
		{
			obj[i].checked=!obj[i].checked
		}
	}else{
		
		obj.checked=!obj.checked
	}
}
function chkCheckBox(obj)
{
    var result=0
    if(obj.length)
    {
        for(var i=0;i<obj.length;i++)
        {
            if(obj[i].checked)
            {
                result++
            }
        }
    }else{
        result=obj.checked?1:0
    }
    return result
}

//函数：删除资源
//参数：表单对像,是否真实删除
function DeleteReco(obj,eventObj,mRealDel)
{
	var selNum
		selNum=chkCheckBox(obj.Id)
	if(selNum==0)
	{
		alert("请选择你要[彻底删除]的资源")
		return false
	}
	if(confirm("你确定要[彻底删除]选中的（"+selNum+"）条资源？"))
	{
		obj.Work.value="DelReco"
		obj.RealDel.value=mRealDel
		eventObj.disabled=true
		obj.submit()
	}
}
//函数：审核资源
//参数：表单对像
function CheckReco(obj,eventObj)
{
    var selNum
        selNum=chkCheckBox(obj.Id)
    if(selNum==0)
    {
        alert("请选择你要[审核]的资源")
        return false
    }
    obj.Work.value="CheckReco"
    eventObj.disabled=true
    obj.submit()
}