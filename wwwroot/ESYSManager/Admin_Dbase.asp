﻿<%@language=vbscript codepage=65001%>
<%Option Explicit%>
<%
Response.CodePage=65001
Response.Charset="UTF-8"
Session.CodePage = 65001
'**************************************************************
' Software Name: EasyesESYS
' Web: http://www.Easyes.com.cn
'**************************************************************
Const DbasePath="DataBackup/"
response.buffer=true	
Const PurviewLevel=1    '操作权限
%>
<!--#include file="Config/ChkPurview.asp"-->
<!--#include file="Config/Config.asp"-->
<!--#include file="Config/Conn.asp"-->
<!--#include file="Config/function.asp"--><%
Dim Act
Act=Request.Form("Act")
Select case Act
Case "Backup"
	Call backuping
Case "Recovery"
	Call Recoverying
End Select
'恢复数据库
Sub Recoverying
Dim NewDbName,oldDbName
	oldDbName=Trim(Request.Form("DataBasePath"))
Dim MyFileObject	
	Set MyFileObject=Server.CreateObject(UpFileFSO)
Dim pathDbName
	pathDbName=Server.Mappath(oldDbName)
IF MyFileObject.FileExists(pathDbName) then
Dim Backupinfo
	Backupinfo=FileCopy(oldDbName,DBFileName,1)
	If Backupinfo=true Then	
  		  WriteErrMsg "·已经成功对数据库恢复!\n"
	Else
  		  WriteErrMsg "·出错!请返回检查\n"
	End iF
	Set Fso=Nothing
Else
	Call WriteErrMsg("·文件不存在！请检查输入是否正确\n")
End If
End Sub

'备份数据库
Sub Backuping
	Dim FSO,BakDBName,Backupinfo
	BakDBName=DbasePath & "#"&date()&".MDB"
	Backupinfo=FileCopy(DBFileName,BakDBName,1)
	Set Fso=Nothing 
	If Backupinfo=true Then	
  		  WriteSuccessMsg "·已经成功对进行备份！"
	Else
  		  WriteErrMsg "·出错!请返回检查\n"
	End iF
End Sub
%><html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>易讯网络建站管理系统--数据库压缩与备份</title>
<meta name="Version" content="<%=SystemVer%>">
<link href="Images/Style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="Config/Button_Skin.js"></script>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tabTitleBar" height="26"><tr><td width="20" align="center"><img border="0" src="Images/file.png"></td><td>当前位置 &gt; 数据库管理</td></tr></table>
<form name="form1" action="" method="post">
<input name="Act" type="hidden" id="Act" value="backup">
<table cellspacing="1" cellpadding="2" class="tabBgColor" width="100%">
<tr>
<td class="tabHeadFont">注：系统自动将数据库备份到“<%=DbasePath%>”目录,名为 <font color="#006600">#<%=Date()%>.MDB</font> 的文件,会复盖同一天的备份文件。</td> 
</tr>
<tr>
<td class="tdbgFont">备份的数据库路径：<input name="backup" type="submit" onClick="form1.Act.value='Backup';submit();" value="备份 " class="button02-out"></td> 
</tr>
</table>
</form>
<form action="Admin_Dbase.asp?" Name="form2" method="post">
<table cellspacing="1" cellpadding="2" class="tabBgColor">
<tr>
<td class="tdbgFont">恢复的数据库路径：</td>
<td class="tdbg">
<input name="DataBasePath" type="text" id="DataBasePath" size="37" class="input_text" value="<%=DbasePath%>">            
<input name="recovery" type="submit" id="recovery" value="恢复" class="button02-out">
<input name="Act" type="hidden" id="Act2" value="Recovery"></td>
</tr>
</table>
</form>
</body>
</html>