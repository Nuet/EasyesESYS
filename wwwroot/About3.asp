﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="zh-cn" />
<meta name="author" content="EasyesESYS" />
<meta name="Copyright" content="Easyes.com.cn" />
<meta name="Description" content="易讯网络国内顶级企业建站软件开发商， 是目前中国ASP建站综合服务提供商之一。" />
<title>易讯建站管理系统(EasyesESYS)-http://www.Easyes.com.cn</title>
<link href="Images/about.css" rel="stylesheet" type="text/css" media="screen" />
</head>
<body>
<div id="main">
<!--#include file="Head.asp"-->
<div class="banner">
	<img alt="易讯建站系统" src="Images/apply_index.jpg" height="112" width="950" /></div>
<div class="wrap">
<div class="sidebar">
<dl class="list_menu">
<dt>关于我们</dt>
<dd><a href="About.asp">公司简介</a></dd>
<dd><a href="About1.asp">联系我们</a></dd>
<dd><a href="About2.asp">招贤纳士</a></dd>
<dd class="on"><a href="About3.asp">企业文化</a></dd>
</dl>
</div>
<div class="const_main">
<div class="gb_mode_title"><h3>企业文化</h3><div class="fun_r"><a href="#">更多</a></div></div>
<div class="main_txt">
<!--企业文化:开始-->
<p>目前，在中国，创业者通常要面临很多挑战，比如，缺少创业和管理经验、欠缺初期启动资金、难以吸引卓越技术人才等，诸多因素使得创业实际成功率并不高。而美国硅谷这个创业者的摇篮，已经形成了从天使投资到中、后期风险投资的完整的、流水线式的体系。一个好的想法哪怕是在概念验证期都可以获得10～50万美元天使资金的支持。而天使投资人又往往是前成功创业者或公司高管，有着深厚的业界经验和背景，除了资金之外能够给创新公司带来他们的视野和业界关系，导入专业人员和管理经验，之后风险投资会对天使项目后期跟进。2008年，美国的天使投资占风险投资总体盘子的40%至50%，共有26万多个活跃的天使投资人和组织。然而在中国，虽然2000年以来风险投资快速发展，但是天使投资群体还未成熟，天使投资领域仍是一大块空白。风险投资苦于找不到足够多的、后期的好项目，创业者苦于找不到足够的早期支持。</p>
<p>像美国这样一个大规模的天使投资群体在中国不是能够一蹴而就的，创新工场的诞生就是要填补这个空白，带来规模化、产业化的天使投资，用一套完整、成熟的体系，甄选出最优秀的创意、创业者、工程师，把每一个创业环节和资源进行最佳整合，帮助创业者，确保其初期的良性发展。我们希望开创出最有市场价值和商业潜力的项目，加以研发和市场运营。当项目成熟到一定程度后，自然剥离母公司成为独立子公司，直至最后上市或被收购。我希望能够建立&ldquo;天使投资 + 创新产品构建&rdquo;这样一个全新的创业投资模式。</p>
<p>创新工场是一个全方位的创业平台，旨在培育创新人才和新一代高科技企业。创新工场将立足信息产业最热门领域：即互联网、移动互联网和云计算，选择相关技术作为创业起点。</p>
<!--企业文化:结束-->
</div>			
</div>
</div>
<!--#include file="Bottom.asp"-->
</div>
</body>
</html>
